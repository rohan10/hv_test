import { valueCalculator } from './finance'
// import { isFloat, isInteger, formatMoney } from './number'

it('Calculates value properly', () => {
  /**
   * export const valueCalculator = (
   *  initialInvestment,
   *  monthlyInvestment,
   *  monthsRenting
   *  ) => initialInvestment + monthlyInvestment * monthsRenting
   */

  expect(valueCalculator(100, 1, 10)).toBe(110)
  expect(() => valueCalculator('100', 1, 10)).toThrow()
  expect(() => valueCalculator(100, '1', 10)).toThrow()
  expect(() => valueCalculator(100, 1, '10')).toThrow()
  expect(() => valueCalculator(100, 1, 10.1)).toThrow()
})
