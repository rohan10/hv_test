import numeral from 'numeral'

export const isFloat = n => n === +n && n !== (n | 0)
export const isInteger = n => n === +n && n === (n | 0)
export const isNumber = n => isFloat(n) || isInteger(n)

export const formatMoney = (val = 0) => {
  if (!(isFloat(val) || isInteger(val))) throw new Error('Invalid input')
  return numeral(val).format('$0,0.00')
}
