import { isNumber, isInteger } from './number'

export const valueCalculator = (
  initialInvestment = 0,
  monthlyInvestment = 0,
  monthsRenting = 0
) => {
  if (!isNumber(initialInvestment))
    throw new Error('Initial investment must be a number')
  if (!isNumber(monthlyInvestment))
    throw new Error('Monthly investment must be a number')
  if (!isInteger(monthsRenting)) throw new Error('Months should an integer')

  return initialInvestment + monthlyInvestment * monthsRenting
}
