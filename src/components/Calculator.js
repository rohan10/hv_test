import React, { useState } from 'react'
import styled from 'styled-components'

import SliderBox from './SliderBox'
import Caption from './Caption'
import Em from './Em'

import { formatMoney } from '../utils/number'
import { valueCalculator as calculateOwnership } from '../utils/finance'

const Wrapper = styled.div`
  width: 480px;
  margin: 40px auto;
  border: 1px solid #e1e1e1;
  border-radius: 3px;
  padding: 0 0 20px;
`

const Value = styled.div`
  font-size: 1.4rem;
  font-weight: bold;
`

const Strike = styled.span`
  text-decoration: line-through;
`

const TotalBox = styled.div`
  border-bottom: 1px solid #e1e1e1;
  padding-bottom: 20px;
  margin-bottom: 20px;
  padding: 20px;
  text-align: left;
`

const Total = styled(Em).attrs({
  as: 'div',
})`
  font-size: 2rem;
  font-weight: bold;
`

const Calculator = ({ rent = 0 }) => {
  const initial = rent * 2
  const [val, setInitial] = useState(initial)
  const [monthly, setMonthly] = useState(0)
  const [months, setMonths] = useState(12 * 5)

  const total = calculateOwnership(val, monthly, months)

  return (
    <Wrapper>
      <TotalBox>
        <Caption>Monthly Payment</Caption>
        <Value>
          {formatMoney(rent)} rent +<Em>{formatMoney(val)} investment</Em>
        </Value>
      </TotalBox>
      <SliderBox
        value={val}
        onChange={setInitial}
        formatter={formatMoney}
        min={initial}
        max={initial * 12}
        step={25}>
        <Strike>Security Deposit</Strike> &rarr; Initial Investment
      </SliderBox>
      <SliderBox
        value={monthly}
        onChange={setMonthly}
        formatter={formatMoney}
        max={500}
        step={5}>
        Monthly Investment
      </SliderBox>
      <SliderBox
        value={months / 12}
        onChange={val => setMonths(val * 12)}
        formatter={val => `${val} ${Math.floor(val) > 1 ? 'years' : 'year'}`}
        min={1}
        max={10}>
        Years with Homevest
      </SliderBox>
      <SliderBox last>Ownership value</SliderBox>
      <Total>{formatMoney(total)}</Total>
      <p>
        Use your {formatMoney(total)} ownership stake as a down payment on any
        home, transfer it to your next Homevest Rental, or cash out.
      </p>
    </Wrapper>
  )
}

export default Calculator
