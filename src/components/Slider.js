import React from 'react'
import BaseSlider from 'rc-slider'
import 'rc-slider/assets/index.css'

const Slider = ({
  min = 0,
  max = 100,
  step = 1,
  value,
  onChange = console.log,
}) => (
  <BaseSlider
    step={step}
    min={min}
    max={max}
    value={value}
    onChange={onChange}
  />
)

export default Slider
