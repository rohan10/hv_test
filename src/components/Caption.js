import styled from 'styled-components'

export default styled.div`
  font-family: sans-serif;
  font-weight: bold;
  margin-bottom: 20px;
  color: #777;
  text-transform: uppercase;
  text-align: left;
  font-size: 0.85rem;
`
