import React from 'react'
import styled from 'styled-components'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { nord } from 'react-syntax-highlighter/dist/esm/styles/hljs'

const Wrapper = styled.div`
  margin: 40px auto;

  font-size: 0.85rem;

  width: 640px;
  text-align: left;
  line-height: 1.45;
`

export default ({ code }) => (
  <Wrapper>
    <SyntaxHighlighter language="javascript" style={nord}>
      {code}
    </SyntaxHighlighter>
  </Wrapper>
)
