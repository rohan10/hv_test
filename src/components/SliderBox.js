import React from 'react'
import styled from 'styled-components'

import Caption from './Caption'
import Slider from './Slider'

const Wrapper = styled.div(
  props => `
  padding: 0 20px 40px;
  margin: 0 20px;
  border-left: 1px solid #e1e1e1;
  position: relative;
  text-align: left;

  &::before {
    position: absolute;
    left: -8px;
    display: block;
    content: ' ';
    border-radius: 999px;
    height: 16px;
    width: 16px;
    background: grey;
  }

  ${props.last &&
    `
    border-left: 0;
    padding-bottom: 0;
  `}
`
)

const Value = styled.div`
  margin-bottom: 20px;
  font-weight: bold;
  font-size: 1.2rem;
  text-align: center;
`

export default ({
  children,
  value,
  onChange,
  formatter = val => val,
  last,
  min,
  max,
  step,
}) => (
  <Wrapper last={last}>
    <Caption>{children}</Caption>
    {Number.isInteger(value) && (
      <>
        <Value>{formatter(value)}</Value>
        <Slider
          value={value}
          onChange={onChange}
          min={min}
          max={max}
          step={step}
        />
      </>
    )}
  </Wrapper>
)
