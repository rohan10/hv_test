import styled from 'styled-components'

export default styled.div`
  font-family: sans-serif;
  font-weight: bold;
  margin-bottom: 20px;
`
