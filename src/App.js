import React from 'react'
import './App.css'

import Calculator from './components/Calculator'
import Code from './components/Code'

const html = `
/**
 * This is an async function, so I assume it'll be used as
 * if it's returning a promise.
 **/

const batchCalls = async (arr = [], concur = 1) => {
  let results = []

  // Slice needed functions
  const fns = arr.slice(0, concur)
  const vals = await Promise.all(fns.map(async fn => await fn()))
  results = results.concat(vals)

  // Recurse
  if (arr.length >= concur) {
    const innerResults = await batchCalls(arr.slice(concur), concur)
    results = results.concat(innerResults)
  }

  return results
}

/**
 * Tested like:
 **/

/*
> const fn1 = () => {
...   console.log(+Date.now(), 'Promise foo has returned')
...   return Promise.resolve('foo')
... }
undefined
> const fn2 = () => {
...   console.log(+Date.now(), 'Promise bar has returned')
...   return Promise.resolve('bar')
... }
undefined
> const fn3 = () => {
...   console.log(+Date.now(), 'Promise baz has returned')
...   return Promise.resolve('baz')
... }
undefined
> const fn4 = () => {
...   console.log(+Date.now(), 'Promise bash has returned')
...   return Promise.resolve('bash')
... }
undefined
> const fn5 = () => {
...   console.log(+Date.now(), 'Promise rohan has returned')
...   return Promise.resolve('rohan')
... }
undefined
> const fn6 = () => {
...   console.log(+Date.now(), 'Promise nair has returned')
...   return Promise.resolve('nair')
... }
undefined
> const promiseArr = [fn1, fn2, fn3, fn4, fn5, fn6]
undefined
> batchCalls(promiseArr, 2)
1574620884428 Promise foo has returned
1574620884428 Promise bar has returned
Promise { <pending> }
> 1574620884432 Promise baz has returned
1574620884432 Promise bash has returned
1574620884432 Promise rohan has returned
1574620884432 Promise nair has returned

>
*/
`

function App() {
  return (
    <div className="App">
      <Code code={html} />
      <hr />
      <Calculator rent={1450} />
    </div>
  )
}

export default App
